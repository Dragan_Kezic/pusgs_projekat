import { Component, OnInit } from '@angular/core';
import { User } from '../models/User';
import { ToastrService } from 'ngx-toastr';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  user: User = new User();
  confirmedPassword = '';

  constructor(
    private toastr: ToastrService
  ) {}

  ngOnInit(): void {
  }

  register(){
    if (
      !this.user.username.length ||
      !this.user.email ||
      !this.user.password.length ||
      !this.user.firstName.length ||
      !this.user.lastName.length ||
      !this.user.dateOfBirth ||
      !this.user.address.length ||
      !this.user.role.length
    ) {
      this.toastr.warning('Warning', 'All fields are required');
      return;
    }

    if(!this.validateEmail()){
      this.toastr.warning('Warning', 'Invalid email');
      return;
    }

    if(!this.validatePassword()){
      this.toastr.warning('Warning', 'Password requires minimal 8 characters');
      return;
    }

    if (this.confirmedPassword !== this.user.password) {
      this.toastr.warning('Warning', 'Passwords do not match');
      return;
    }

    if(!this.validateDateOfBirth()){
      this.toastr.warning('Warning', 'A user must be over 13 years old to use the site');
      return;
    }

    if(!this.validateName()){
      this.toastr.warning('Warning', 'First name must contain only letters and spaces');
      return;
    }

    if(!this.validateSurname()){
      this.toastr.warning('Warning', 'Last name must contain only letters and spaces');
      return;
    }

    /*Call server*/

  }

  validateEmail(): boolean{
    const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    let arr = this.user.email.match(re);
    if(arr == null || arr[0] != this.user.email){
      return false;
    }
    return true;
  }

  validatePassword(): boolean{
    return this.user.password.length >= 8;
  }

  validateDateOfBirth(): boolean{
    var year1 = new Date().getFullYear();
    var year2 = new Date(this.user.dateOfBirth).getFullYear();
    return Math.abs(year1 - year2) > 13;
  }

  validateName(): boolean{
    const re = /([A-Za-z][ ]{0,1})+/;
    let arr = this.user.firstName.match(re);
    if(arr == null || arr[0] != this.user.firstName){
      return false;
    }
    return true;
  }

  validateSurname(): boolean{
    const re = /([A-Za-z][ ]{0,1})+/;
    let arr = this.user.lastName.match(re);
    if(arr == null || arr[0] != this.user.lastName){
      return false;
    }
    return true;
  }

}
