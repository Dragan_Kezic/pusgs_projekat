export enum WorkOrderStatus {
    Draft = "DRAFT",
    Submitted = "SUBMITTED",
    Approved = "APPROVED",
    Declined = "DECLINED",
    Canceled = "CANCELLED"
}