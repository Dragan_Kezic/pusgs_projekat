export enum Role {
    Worker = "WORKER",
    TeamMember = "TEAM_MEMBER",
    Dispatcher = "DISPATCHER",
    Admin = "ADMIN"
}