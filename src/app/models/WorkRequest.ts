import { Incident } from "./Incident";
import { User } from "./User";
import { WorkOrderStatus } from "./WorkOrderStatus";
import { DocumentType } from "./DocumentType";

export class WorkRequest{
    id: string;
    
    constructor(
        public documentType: DocumentType,
        public status: WorkOrderStatus,
        public incident: Incident,
        public address: string,
        public startDate: Date,
        public endDate: Date,
        public user: User,
        public purpose: string,
        public notes: string,
        public urgency: boolean,
        public company: string,
        public phone: string,
        public creationDate: Date,
    ){ this.id="0"}
}