export enum DocumentType {
    PlannedWork = "PLANNED_WORK",
    UnplannedWork = "UNPLANNED_WORK"
}