import { Role } from "./Role";
export class User {
    id: string;

    constructor(
        public username: string = '',
        public email: string = '',
        public password: string = '',
        public firstName: string = '',
        public lastName: string = '',
        public dateOfBirth: Date = new Date(),
        public address: string = '',
        public role: Role = Role.Worker,
        public status: boolean = true,
    ){}
  }