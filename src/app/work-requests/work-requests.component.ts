import { Component, OnInit, ViewChild } from '@angular/core';
import { DocumentType } from '../models/DocumentType';
import { Role } from '../models/Role';
import { User } from '../models/User';
import { WorkOrderStatus } from '../models/WorkOrderStatus';
import { WorkRequest } from '../models/WorkRequest';
import { MatTableDataSource} from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort, Sort} from '@angular/material/sort';


@Component({
  selector: 'app-work-requests',
  templateUrl: './work-requests.component.html',
  styleUrls: ['./work-requests.component.css']
})
export class WorkRequestsComponent implements OnInit {

  @ViewChild(MatPaginator) paginator: MatPaginator;

  allRequests: WorkRequest[];
  requests:  MatTableDataSource<WorkRequest>;
  constructor() { }

  ngOnInit(): void {
    const users = [
      new User("Luka","mocki1997@gmail.com","lukaluka","Luka","Kosovic",new Date(),"Podgoricka 6",Role.Worker,true),
      new User("Ana","ana2000@gmail.com","anaanaana","Ana","Kosovic",new Date(),"Podgoricka 6",Role.Worker,true),
    ];
    const requests = [
      new WorkRequest(
        DocumentType.PlannedWork,
        WorkOrderStatus.Draft,
        null,
        "Mose Pijade 59",
        new Date(),
        new Date(),
        users[0],
        "some purpose",
        "some text",
        false, /* urgency */
        "Company X",
        "0617996627",
        new Date(),
      ),
      new WorkRequest(
        DocumentType.PlannedWork,
        WorkOrderStatus.Draft,
        null,
        "Marsala Tita 82",
        new Date(),
        new Date(),
        users[0],
        "some purpose",
        "some text",
        true, /* urgency */
        "Company X",
        "0617996627",
        new Date(),
      ),
      new WorkRequest(
        DocumentType.UnplannedWork,
        WorkOrderStatus.Submitted,
        null,
        "Podgoricka 6",
        new Date(),
        new Date(),
        users[0],
        "some purpose",
        "some text",
        false, /* urgency */
        "Company Y",
        "0617996627",
        new Date(),
      ),
      new WorkRequest(
        DocumentType.PlannedWork,
        WorkOrderStatus.Draft,
        null,
        "Zarka Zrenjanina 82",
        new Date(),
        new Date(),
        users[1],
        "some purpose",
        "some text",
        true, /* urgency */
        "Company Y",
        "0638383758",
        new Date(),
      ),
      new WorkRequest(
        DocumentType.PlannedWork,
        WorkOrderStatus.Draft,
        null,
        "Mose Pijade 42",
        new Date(),
        new Date(),
        users[1],
        "some purpose",
        "some text",
        true, /* urgency */
        "Company W",
        "0638383758",
        new Date(),
      ),
      new WorkRequest(
        DocumentType.UnplannedWork,
        WorkOrderStatus.Submitted,
        null,
        "Ivana Milutinovica 2",
        new Date(),
        new Date(),
        users[0],
        "some purpose",
        "some text",
        false, /* urgency */
        "Company Z",
        "0638383758",
        new Date(),
      ),
    ];
    this.allRequests = requests;
    this.requests = new MatTableDataSource(requests);
  }

  ngAfterViewInit() {
    this.requests.paginator = this.paginator;
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim(); // Remove whitespace
    filterValue = filterValue.toLowerCase(); // Datasource defaults to lowercase matches
    this.requests.filter = filterValue;
  }

  sortData(sort: Sort) {

    if (!sort.active || sort.direction === '') {
      return;
    }

    this.requests.data = this.requests.data.sort((a: any, b: any) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'id': return this.compare(a.id, b.id, isAsc);
        case 'startDate': return this.compare(a.startDate + '', b.startDate + '', isAsc);
        case 'phone': return this.compare(a.phone, b.phone, isAsc);
        case 'status': return this.compare(a.status, b.status, isAsc);
        case 'address': return this.compare(a.address, b.address, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  showAll(){
    this.requests.data = this.allRequests;
  }

  showMine(){
    this.requests.data = this.allRequests.filter(x => x.user.username == "Luka"); /* Replace username with id*/
  }
}