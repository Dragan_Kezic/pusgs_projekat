import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HomePageComponent } from './home-page/home-page.component';
import { WorkRequestsComponent } from './work-requests/work-requests.component';
import { AdminBoardComponent } from './admin-board/admin-board.component';
import { WorkPlanComponent } from './work-plan/work-plan.component';
import { SecurityDocumentComponent } from './security-document/security-document.component';
import { NewWorkRequestComponent } from './new-work-request/new-work-request.component';   

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'work-requests',
    component: WorkRequestsComponent
  },
  {
    path: 'work-plan',
    component: WorkPlanComponent
  },
  {
    path: 'security-document',
    component: SecurityDocumentComponent
  },
  {
    path: 'admin-board',
    component: AdminBoardComponent
  },
  {
    path: 'new-work-request',
    component: NewWorkRequestComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
