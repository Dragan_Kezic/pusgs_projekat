import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  username: string;
  password: string;

  constructor(private toastr: ToastrService) { }

  ngOnInit(): void {
    this.username = '';
    this.password = '';
  }

  login(){
    if(!this.validateUsername()){
      this.toastr.warning("All fields are required", "Warning");
      return;
    }
    if(!this.validatePassword()){
      this.toastr.warning("Password requires minimum 8 characters","Warning");
      return;
    }

    /*Call server*/

  }

  validateUsername(): boolean{
    return this.username.length > 0;
  }

  validatePassword():boolean {
    return this.password.length >= 8;
  }

}
