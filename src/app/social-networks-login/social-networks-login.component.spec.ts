import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialNetworksLoginComponent } from './social-networks-login.component';

describe('SocialNetworksLoginComponent', () => {
  let component: SocialNetworksLoginComponent;
  let fixture: ComponentFixture<SocialNetworksLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialNetworksLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialNetworksLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
