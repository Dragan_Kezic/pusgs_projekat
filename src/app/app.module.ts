import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ToastrModule } from 'ngx-toastr';
import { FormsModule } from '@angular/forms';
import { AdminBoardComponent } from './admin-board/admin-board.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SocialNetworksLoginComponent } from './social-networks-login/social-networks-login.component';
import { WorkRequestsComponent } from './work-requests/work-requests.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { WorkPlanComponent } from './work-plan/work-plan.component';
import { SecurityDocumentComponent } from './security-document/security-document.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { NewWorkRequestComponent } from './new-work-request/new-work-request.component';
import { MatToolbarModule } from  '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
    AdminBoardComponent,
    HomePageComponent,
    SocialNetworksLoginComponent,
    WorkRequestsComponent,
    WorkPlanComponent,
    SecurityDocumentComponent,
    NewWorkRequestComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    FormsModule,
    NgbModule,
    MatTooltipModule,
    MatPaginatorModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatSortModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatIconModule,
    MatDividerModule,
  ],
  providers: [],
  bootstrap: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    HeaderComponent,
  ]
})
export class AppModule { }
