import { Component } from '@angular/core';
import { AuthenticationService } from '../services/authentication/authentication.service';
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  constructor(
    private router: Router,
    /*private authentication: AuthenticationService*/
  ) {}

  isLoggedIn(): boolean {
    return true;
    //return this.authentication.getIsLoggedIn();
  }

  isAdmin(): boolean {
    return true;
    //return this.authentication.isAdmin();
  }

  logOut() {
    /*this.authentication.logOut();*/
    this.router.navigate(['/login']);
  }

}
