import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { User } from '../models/User';
import { Role } from '../models/Role';

@Component({
  selector: 'app-admin-board',
  templateUrl: './admin-board.component.html',
  styleUrls: ['./admin-board.component.css']
})
export class AdminBoardComponent implements OnInit {

  newUsers: User[] = [];

  constructor(private toastrService: ToastrService) { }

  ngOnInit(): void {
    this.addRandomUsers();
  }

  approve(email: string){
    console.log("approve");
    /*Call server*/
  }

  reject(email: string){
    console.log("reject");
    /*Call server*/
  }

  addRandomUsers(){
    this.newUsers = [
      new User("Luka","mocki1997@gmail.com","lukaluka","Luka","Kosovic",new Date(),"Podgoricka 6",Role.Worker,true),
      new User("Ana","ana2000@gmail.com","anaanaana","Ana","Kosovic",new Date(),"Podgoricka 6",Role.Worker,true),
    ];
  }

}
